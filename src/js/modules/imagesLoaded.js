export default class {
  constructor(el) {
    this.images = document.querySelectorAll(el + ' img');
    this.catchEvents();
    console.log(this.images);
  }
  
  hideImages(i) {
    console.log('hide');
    this.images[i].parentNode.classList.add('is-hidden-image');
  }
  
  catchEvents() {
    for (let i = 0; i < this.images.length; i++) {
      this.hideImages(i);
      this.images[i].onload = this.onSuccess.bind(this, i);
      this.images[i].onerror = this.onError.bind(this, i);
    }
    
  }
  
  onSuccess(i) {
    this.images[i].parentNode.classList.remove('is-hidden-image');
    this.images[i].parentNode.classList.add('is-loaded');
  }
  
  onError(i) {
    this.images[i].parentNode.classList.add('is-broken');
  }
  
}