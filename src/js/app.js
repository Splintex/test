import Rate from './modules/rate';
import ImagesLoaded from './modules/imagesLoaded';


document.addEventListener("DOMContentLoaded", function () {
  
  new Rate({
    selector: '.js-rate',
    star: './img/star.svg',
    starActive: './img/star-active.svg'
  });
  
  new Rate({
    selector: '.js-rate-big',
    star: './img/star2.svg',
    starActive: './img/star2-active.svg',
    size: 40
  });
  
  new ImagesLoaded('.js-images-loaded');
  
});

