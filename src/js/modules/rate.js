export default class {
  constructor(options) {
    this.el = document.querySelectorAll(options.selector);
    this.star = options.star;
    this.starActive = options.starActive;
    this.size = options.size || 18;
    this.createDOM();
    this.bindEvents();
  }
  
  createDOM() {
    for (let i = 0; i < this.el.length; i++) {
      const value = this.getRate(i);
      const name = +new Date();
      this.el[i].insertAdjacentHTML('afterBegin', `<div class="rate"></div>`);
      for (let j = 1; j <= 5; j++) {
        let label = `
              <label class="rate__item">
                  <input class="js-rate-radio" type="radio" name="rate-${name}" value="${j}">
                  <span class="rate__alt">${j}</span>
                  <img src="${this.star}" alt="${j}" width="${this.size}" title="${j}">
              </label>`;
        if (value > j) {
          label = `
            <label class="rate__item is-active">
                <input class="js-rate-radio" type="radio" name="rate-${name}" value="${j}">
                 <span class="rate__alt">${j}</span>
                <img src="${this.starActive}" alt="${j}" width="${this.size}" title="${j}">
            </label>`;
        }
        if (value == j) {
          label = `
            <label class="rate__item is-active">
                <input class="js-rate-radio" type="radio" name="rate-${name}" value="${j}" checked>
                 <span class="rate__alt">${j}</span>
                <img src="${this.starActive}" alt="${j}" width="${this.size}" title="${j}">
            </label>`;
        }
        this.el[i].firstChild.insertAdjacentHTML('beforeEnd', label);
      }
    }
  }
  
  getRate(i) {
    return this.el[i].dataset.value || null;
  }
  
  bindEvents() {
    for (let i = 0; i < this.el.length; i++) {
      this.el[i].addEventListener('click', this.changeRate.bind(this))
    }
  }
  
  changeRate(event) {
    if (event.target.tagName == 'INPUT') {
      let radio = event.target;
      let labels = radio.parentElement.parentNode.children;
      for (let i = 0; i < labels.length; i++) {
        labels[i].classList.remove('is-active');
        labels[i].lastElementChild.setAttribute('src', this.star);
      }
      for (let i = 0; i < radio.value; i++) {
        labels[i].classList.add('is-active');
        labels[i].lastElementChild.setAttribute('src', this.starActive);
      }
    }
  }
}